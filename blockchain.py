# initilize blockchain list
from functools import reduce
from collections import OrderedDict
import json

from hash_utils import hash_block, hash_string_sha256

MINNING_REWARD = 10

block_chain = []

open_transaction = []
owner = "Rumes"
participent = set({"Rumes"})


def load_data():
    global block_chain
    global open_transaction
    try:
        with open("blockchain.txt", mode="r") as f:
            file_content = f.readlines()

            block_chain = json.loads(file_content[0][:-1])
            block_chain_converted = []
            for block in block_chain:
                updated_block = {
                    "hash": block["hash"],
                    "index": block["index"],
                    "transaction": [OrderedDict([("sender", tx["sender"]), ("recipient", tx["recipient"]), ("amount", tx["amount"])]) for tx in block["transaction"]],
                    "proof": block["proof"]
                }
                block_chain_converted.append(updated_block)
            block_chain = block_chain_converted

            open_transaction = json.loads(file_content[1])
            open_transaction_converted = []
            for transaction in open_transaction:
                converted_transaction = OrderedDict(
                    [("sender", transaction["sender"]), ("recipient", transaction["recipient"]), ("amount", transaction["amount"])])

                open_transaction_converted.append(converted_transaction)
            open_transaction = open_transaction_converted
    except IOError:

        genisis_block = {
            "hash": "",
            "index": 0,
            "transaction": [],
            "proof": 100
        }

        block_chain.append(genisis_block)


load_data()


def save_data():
    try:
        with open("blockchain.txt", mode="w") as f:
            f.write(json.dumps(block_chain))
            f.write("\n")
            f.write(json.dumps(open_transaction))
    except IOError:
        print("Saving Faild!")

def get_blockchain_last_element():
    """ get blockchain list last element """
    if len(block_chain) < 1:
        return None
    return block_chain[-1]


def valid_proof(transaction, last_hash, proof):
    guess = (str(transaction) + str(last_hash) + str(proof)).encode()

    guess_hash = hash_string_sha256(guess)
    return guess_hash[0:3] == "000"


def proof_of_work():
    last_block = block_chain[-1]
    last_hash = hash_block(last_block)
    proof = 0
    while not valid_proof(open_transaction, last_hash, proof):
        proof += 1
    return proof


def get_balance(participent):
    tx_send_amount = [[tx_amount['amount'] for tx_amount in block['transaction']
                       if tx_amount['sender'] == participent] for block in block_chain]
    open_transaction_amount = [tx_amount['amount']
                               for tx_amount in open_transaction if tx_amount['sender'] == participent]

    tx_send_amount.append(open_transaction_amount)

    send_value = reduce(lambda tx_sum, tx_amnt: tx_sum + sum(tx_amnt)
                        if len(tx_amnt) > 0 else tx_sum + 0, tx_send_amount, 0)

    tx_recived_amount = [[tx_amount['amount'] for tx_amount in block["transaction"]
                          if tx_amount['recipient'] == participent] for block in block_chain]
    recive_value = reduce(lambda tx_sum, tx_amnt: tx_sum + sum(tx_amnt)
                          if len(tx_amnt) > 0 else tx_sum + 0, tx_recived_amount, 0)

    return recive_value - send_value


def verify_transaction(transaction):
    return get_balance(transaction['sender']) > transaction['amount']


def add_transactions(recipient, sender=owner, amount=1.0):
    """ Add new transaction to blockchain list:

            arguments:
                amount: transaction amount
                recipient: identity of transaction reciver
                sender:  identity of transaction sender
     """

    transaction = OrderedDict(
        [("sender", sender), ("recipient", recipient), ("amount", amount)])

    if(verify_transaction(transaction)):
        open_transaction.append(transaction)
        save_data()
        participent.add(sender)
        participent.add(recipient)
        return True
    return False


def empty_open_transaction():
    global open_transaction

    if len(open_transaction) > 0:
        open_transaction = []


def mine_block():
    last_block = block_chain[-1]
    hashed_block = hash_block(last_block)

    proof = proof_of_work()

    reward_block = OrderedDict(
        [("sender", "MINNING"), ("recipient", owner), ("amount", MINNING_REWARD)])

    coppied_transaction = open_transaction[:]
    empty_open_transaction()
    save_data()
    coppied_transaction.append(reward_block)
    block = {
        "hash": hashed_block,
        "index": len(block_chain),
        "transaction": coppied_transaction,
        "proof": proof
    }

    block_chain.append(block)


def get_transaction_amount_user():
    """ Get transaction amount from user and return the value as float. """
    recipient = input("Pleas enter recipient:")
    tx_amount = float(input("Pleas enter transaction amount:"))
    return recipient, tx_amount


def print_blockchain_element():
    """ Print Each and every element in the blockchain"""
    print("-" * 30)
    for block in block_chain:
        print("Blockchain values")
        print(block)
    else:
        print("-" * 30)


def get_user_choice():
    """ Get user choce input as string and return it """
    user_choice = input("Insert your choice :")
    return user_choice


def verify_blockchain():
    """ Verify weather bloakchain is manupulated """
    for (index, block) in enumerate(block_chain):
        if index == 0:
            continue

        if block["hash"] != hash_block(block_chain[index - 1]):
            return False
        if not valid_proof(block["transaction"][:-1], block["hash"], block["proof"]):
            return False
    return True


while True:
    print("Insert your choice !")
    print("1 : Add transactions.")
    print("2 : Mine block")
    print("3 : Print blockchain.")
    print("h : Manupulate blockchain.")
    print("q : Exit fromapplication.")
    user_choice = get_user_choice()
    if user_choice == "1":
        recipient, tx_amount = get_transaction_amount_user()
        if(add_transactions(recipient, amount=tx_amount)):
            print("Transactions success")
        else:
            print("Transactions Faild")
    elif user_choice == "2":
        mine_block()
        save_data()
    elif user_choice == "3":
        print_blockchain_element()
    elif user_choice == "h":
        if len(block_chain) > 0:
            block_chain[0] = {
                "hash": "",
                "index": 0,
                "transaction": [{'sender': 'Rumes', 'recipient': 'randika', 'amount': 12.0}]
            }
    elif user_choice == "q":
        break
    else:
        print("Invalid Argument Insert valid argument")

    if not verify_blockchain():
        print("blockchain not veryfied")
        break

print("Done")
